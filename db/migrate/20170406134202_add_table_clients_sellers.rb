class AddTableClientsSellers < ActiveRecord::Migration
  def change
    create_join_table :clients, :sellers do |t|
      t.index %i[client_id seller_id]
      t.index %i[seller_id client_id]
    end
  end
end
