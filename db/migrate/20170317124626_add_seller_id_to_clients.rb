class AddSellerIdToClients < ActiveRecord::Migration
  def change
    add_column :clients, :seller_id, :integer
  end
end
