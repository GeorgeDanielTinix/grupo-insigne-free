class CreateJoinTableClientCategory < ActiveRecord::Migration
  def change
    create_join_table :clients, :categories do |t|
      # t.index [:client_id, :category_id]
      # t.index [:category_id, :client_id]
    end
  end
end
