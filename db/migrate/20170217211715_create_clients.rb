class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.float :latitude
      t.float :longitude
      t.integer :client_number
      t.string :name
      t.string :address
      t.integer :CP
      t.string :zona
      t.text :description

      t.timestamps null: false
    end
  end
end
