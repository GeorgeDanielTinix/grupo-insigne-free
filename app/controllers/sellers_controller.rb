class SellersController < ApplicationController
  def index
    @sellers = Seller.all
  end

  def new
    @seller = Seller.New
  end

  def create
    @seller = Seller.new(seller_params)

    if @seller.save
      redirect_to @seller
    else
      render 'New'
    end
  end

  def update
    @seller = Seller.find(params[:id])
  end

  def destroy
    @seller = Seller.find(params[:id])
    @seller.destroy

    redirect_to root_path
  end

  private

  def seller_params
    params.require(:seller).permit(:name)
  end
end
