require 'csv'

class Client < ActiveRecord::Base
  has_and_belongs_to_many :categories
  has_and_belongs_to_many :sellers

  validates :name, presence: true
  validates :address, presence: true
  validates :CP, presence: true
  validates :zona, presence: true
  validates :client_number, presence: true

  belongs_to :user
  geocoded_by :address
  after_validation :geocode

  # Array de formatos permitidos
  ALLOW_FORMATS = ['text/csv'].freeze

  # Import CSV File
  def self.import(file)
    if file.blank? || ALLOW_FORMATS.exclude?(file.content_type)
      # Si no enviaron un archivo, o no tiene el formato correcto, devolvemos.
      return nil
    end

    CSV.foreach(file.tempfile.path, headers: true) do |row|
      client_hash = row.to_hash

      if Client.exists?(client_number: client_hash['client_number'])
        client = Client.find_by_client_number(client_hash['client_number'])
        client.update_attributes(client_hash)
      else
        Client.create!(client_hash)
      end
    end
  end

  # Export CSV File
  def self.to_csv(attributes = column_names, options = {})
    attributes = %w[id longitude latitude client_number name address CP zona description vendedores_id seller_id]
    CSV.generate(options) do |csv|
      csv << attributes

      all.each do |client|
        csv << client.attributes.values_at(*attributes)
      end
    end
  end
end
